// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDsVUzKkC291gHqLCkgkBy5dwOxa5Eni3U',
    authDomain: 'finals-project-airline.firebaseapp.com',
    databaseURL: 'https://finals-project-airline.firebaseio.com',
    projectId: 'finals-project-airline',
    storageBucket: 'finals-project-airline.appspot.com',
    messagingSenderId: '924803702305',
    appId: '1:924803702305:web:d2d0dfde9eef5cc33f8d60'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
