import { Component, OnInit } from '@angular/core';

import {
  Router
} from '@angular/router';
@Component({
  selector: 'app-book-flight',
  templateUrl: './book-flight.component.html',
  styleUrls: ['./book-flight.component.css']
})
export class BookFlightComponent implements OnInit {

  private roundTripStatus;
  constructor(
    private router: Router) { }

  ngOnInit() {
  }

  isRoundTrip(){
    const doc = document.getElementById('isRoundTrip') as HTMLInputElement;
    this.roundTripStatus = doc.checked; 
    console.log(this.roundTripStatus);
  }

  
  redirect(destination) {
    this.router.navigate([destination], {
      skipLocationChange: true
    });
  }
}
