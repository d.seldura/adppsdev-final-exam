import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFlightUserComponent } from './view-flight-user.component';

describe('ViewFlightUserComponent', () => {
  let component: ViewFlightUserComponent;
  let fixture: ComponentFixture<ViewFlightUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFlightUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFlightUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
