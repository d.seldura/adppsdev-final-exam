import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

import {
  Router
} from '@angular/router';
@Component({
  selector: 'app-view-flight-user',
  templateUrl: './view-flight-user.component.html',
  styleUrls: ['./view-flight-user.component.css']
})
export class ViewFlightUserComponent implements OnInit {

  private userFlights;

  constructor(
    private firestore: AngularFirestore,
    private router: Router

  ) { }

  ngOnInit() {
    this.getFlights();
  }

  getFlights() {
    const user = JSON.parse(sessionStorage.getItem('userData'));
    let flights: Array < any > = [];
    this.firestore
      .collection('booked-flights')
      .ref
      .where("Owner", "==", user.username )
      .get()
      .then((snapshot) => {
        if (snapshot.empty) {
          console.log('User has no flights');
          return this.userFlights = flights;
        } else {
          snapshot.forEach((doc) => {
            flights.push(doc.data());
          });
          for (let x of flights){
            x.BookDate = x.BookDate.toDate().toString();
            x.ETD_date = (new Date(x.ETD_date)).toString();
            x.ETA_date = (new Date(x.ETA_date)).toString();
          
          }
          this.userFlights = flights;
        }

        console.log(this.userFlights);
      })
      .catch((error) => {
        alert( error );
      });
  }
  
  redirect(destination) {
    this.router.navigate([destination], {
      skipLocationChange: true
    });
  }
}
