import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocateFlightComponent } from './locate-flight.component';

describe('LocateFlightComponent', () => {
  let component: LocateFlightComponent;
  let fixture: ComponentFixture<LocateFlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocateFlightComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocateFlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
