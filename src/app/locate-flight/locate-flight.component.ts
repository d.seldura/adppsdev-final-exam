import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseDBMService } from 'src/app/firebase-dbm.service';
import { Router } from '@angular/router';
import Joi from '@hapi/joi';

@Component({
  selector: 'app-locate-flight',
  templateUrl: './locate-flight.component.html',
  styleUrls: ['./locate-flight.component.css']
})
export class LocateFlightComponent implements OnInit {
  private createFlightSchema = Joi.object({
    Origin: Joi.string()
      .min(3)
      .required(),
    Destination: Joi.string()
      .min(3)
      .required(),
    ETD: Joi.date().required()
  });

  private findFlightForm;
  private selectedFlight;
  private locations;
  private searchResult;

  constructor(
    private formBuilder: FormBuilder, 
    private db: AngularFirestore,
    private dbServ: FirebaseDBMService,
    private router: Router
    ) {
    this.findFlightForm = this.formBuilder.group({
      origin: '',
      destination: '',
      etd: ''
    });
    this.selectedFlight = this.formBuilder.group({
      flight: '',
    });
  }

  onSubmit(flightData) {
    const flight = {
      Origin: flightData.origin,
      Destination: flightData.destination,
      ETD: flightData.etd
    };
    const { error } = this.createFlightSchema.validate(flight);
    if (error !== undefined) {
      return alert(error.details[0].message);
    }
    if (flight.Destination==flight.Origin)
      return alert("Origin and Destination cannot be the same!");
    const retData = [];
    this.db
      .collection('flights')
      .ref
      .where('Origin', '==', flight.Origin)
      .where('Destination', '==', flight.Destination)
      .where('ETD_date','==',flightData.etd)
      .get()
      .then((snapshot) => {
        // console.log(snapshot);
        if (snapshot.empty) {
          this.searchResult = null;
          return console.log('No Flights Found');
        } else {
          snapshot.forEach((doc) => {
            retData.push(doc.data());
          });
          for (let x of retData){
            x.ETD_date = (new Date(x.ETD_date+':'+x.ETD_time)).toLocaleDateString() +' '+ (new Date(x.ETD_date+':'+x.ETD_time)).toLocaleTimeString();
            x.ETA_date = (new Date(x.ETA_date+':'+x.ETA_time)).toLocaleDateString() +' '+ (new Date(x.ETA_date+':'+x.ETA_time)).toLocaleTimeString();
          }
          this.searchResult = retData;
          console.log(retData);
        }
      });
  }

  onReset() {
    const resetForm = document.getElementById('findFlightForm') as HTMLFormElement;
    resetForm.reset();
  }

  cart(data){
    const outFlight = this.searchFlightDataFromResults(data.flight, this.searchResult);

    if(confirm("Book flight "+outFlight.FlightCode+"? Total balance: " + outFlight.Price))
    {
      this.setBooking(outFlight);
      alert("Booking successful");//add redirect
    }
  }
  

  searchFlightDataFromResults(searchParam, collection){
    for (let x of collection){
      if (x.FlightCode==searchParam)
        return x;
    }
  }

  setBooking(flight){
    const random = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5).toUpperCase();
    const user = JSON.parse(sessionStorage.getItem('userData'));
    let data = {  
      BookingID : random,
      BookDate: new Date(),
      Airline: flight.Airline,
      Destination: flight.Destination,
      ETA_date: flight.ETA_date,
      ETA_time: flight.ETA_time,
      ETD_date: flight.ETD_date,
      ETD_time: flight.ETD_time,
      FlightCode: flight.FlightCode,
      Origin: flight.Origin,
      Price: flight.Price,
      Owner: user.username,
      Name: user.identity
    }
    this.dbServ.sendData(data,"booked-flights",data.BookingID);
  }

  ngOnInit() {
    this.locations = JSON.parse(sessionStorage.getItem('destinations'));
  }
}
