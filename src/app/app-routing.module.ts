import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './frontPanels/home/home.component';
import { FlightPanelComponent } from './adminPanel/flight-panel/flight-panel.component';
import { AddFlightComponent } from './adminPanel/flightCRUD/add-flight/add-flight.component';
import { RegisterComponent } from './frontPanels/register/register.component';
import { LoginComponent } from './frontPanels/login/login.component';
import { LoginRegisterComponent } from './frontPanels/login-register/login-register.component';
import { UserProfileComponent } from './userPanel/user-profile/user-profile.component';
import { BookFlightComponent } from './userPanel/book-flight/book-flight.component';
import { AddDestinationComponent } from './adminPanel/add-destination/add-destination.component';
import { ViewFlightUserComponent } from './view-flight-user/view-flight-user.component';
import { UserdataComponent } from './adminPanel/userdata/userdata.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'adminPanel', component: FlightPanelComponent},
  {path: 'userPanel', component: UserProfileComponent},
  {path: 'add-flight', component: AddFlightComponent},
  {path: 'add-destination', component: AddDestinationComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'login-register', component: LoginRegisterComponent},
  {path: 'login', component: LoginComponent},
  {path: 'book-flight', component: BookFlightComponent},
  {path: 'view-flight', component: ViewFlightUserComponent},
  {path: 'userData', component: UserdataComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
