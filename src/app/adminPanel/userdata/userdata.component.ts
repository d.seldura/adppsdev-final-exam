import {
  Component,
  OnInit
} from '@angular/core';
import {
  AngularFirestore
} from '@angular/fire/firestore';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-userdata',
  templateUrl: './userdata.component.html',
  styleUrls: ['./userdata.component.css']
})
export class UserdataComponent implements OnInit {
  private userList;
  private userFlights;
  constructor(
    private firestore: AngularFirestore,
    private router: Router
  ) {}

  ngOnInit() {
    let users: Array < any > = [];
    this.firestore
      .collection('users')
      .ref.get()
      .then((snapshot) => {
        if (snapshot.empty) {
          return console.log('Destinations empty');
        } else {
          snapshot.forEach((doc) => {
            users.push(doc.data());
          });
          console.log("users: ", );
          console.log(users);
        }
        this.userList = users;
      })
      .catch((error) => {
        alert(error);
      });
  }

  viewFlights(username){
    let flights: Array < any > = [];
    this.firestore
      .collection('booked-flights')
      .ref
      .where("Owner", "==", username )
      .get()
      .then((snapshot) => {
        if (snapshot.empty) {
          alert('User has no flights');
          return this.userFlights = flights;
        } else {
          snapshot.forEach((doc) => {
            flights.push(doc.data());
          });
          for (let x of flights){
            x.BookDate = x.BookDate.toDate().toString();
            x.ETD_date = (new Date(x.ETD_date)).toString();
            x.ETA_date = (new Date(x.ETA_date)).toString();
          
          }
          this.userFlights = flights;
        }

        console.log(this.userFlights);
      })
      .catch((error) => {
        alert( error );
      });
  }

  redirect(destination) {
    this.router.navigate([destination], {
      skipLocationChange: true
    });
  }
}
