import { Component, OnInit } from '@angular/core';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-flight-panel',
  templateUrl: './flight-panel.component.html',
  styleUrls: ['./flight-panel.component.css']
})
export class FlightPanelComponent implements OnInit {

  constructor(
    private router: Router
  ) {}
  ngOnInit() {
  }

  
  redirect(destination) {
    this.router.navigate([destination], {
      skipLocationChange: true
    });
  }
}
