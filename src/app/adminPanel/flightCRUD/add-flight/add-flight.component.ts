import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder
} from '@angular/forms';
import {
  FirebaseDBMService
} from 'src/app/firebase-dbm.service';
import {
  Router
} from '@angular/router';
import Joi from '@hapi/joi';

@Component({
  selector: 'app-add-flight',
  templateUrl: './add-flight.component.html',
  styleUrls: ['./add-flight.component.css']
})
export class AddFlightComponent implements OnInit {

  private createFlightSchema = Joi.object({
    FlightCode: Joi.string()
      .min(3)
      .required(),
    Origin: Joi.string()
      .min(3)
      .required(),
    Destination: Joi.string()
      .min(3)
      .required(),
    Airline: Joi.string()
      .min(3)
      .required(),
    ETD_date: Joi.string().required(),
    ETD_time: Joi.string().required(),
    ETA_date: Joi.string().required(),
    ETA_time: Joi.string().required(),
    Price: Joi.number().required()
  });

  private flightForm;
  private randomFlight;
  private locations;
  private airline;


  constructor(
    private formBuilder: FormBuilder,
    private db: FirebaseDBMService,
    private router: Router
  ) {
    this.airline = ['Air Asia', 'Fly Emirates', 'Philippine Airlines', 'Cebu Pacific', 'Tiger Air'];
    this.flightForm = this.formBuilder.group({
      flightCode: '',
      origin: '',
      destination: '',
      etd: '',
      etdTime: '',
      eta: '',
      etaTime: '',
      airline: '',
      price: ''
    });
  };


  onSubmit(flightData) {
    let flight = {
      FlightCode: flightData.flightCode,
      Origin: flightData.origin,
      Destination: flightData.destination,
      ETD_date: flightData.etd,
      ETD_time: flightData.etdTime,
      ETA_date: flightData.eta,
      ETA_time: flightData.etaTime,
      Airline: flightData.airline,
      Price: <number>flightData.price
    };
    console.log(flight);
    const {
      error
    } = this.createFlightSchema.validate(flight);
    if (new Date(flight.ETA_date+':'+flight.ETA_time)<=new Date(flight.ETD_date+':'+flight.ETD_time))
    return this.throwError("Arrival date/time cannot be less than or the same as departure");
    if (error != undefined) return this.throwError(error.details[0].message);
    if (flight.Origin===flight.Destination) {
      this.throwError('Origin cannot be the same as destination');
      return;
    }
    this.db.sendData(flight, 'flights', flight.FlightCode);
    this.throwError("Flight Added");
    this.onReset();
  }

  onReset() {
    const resetForm = document.getElementById('flightForm') as HTMLFormElement;
    resetForm.reset();
    this.randomFlight = this.generateRandomFlightCode();
  }

  generateRandomFlightCode() {
    const random = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5).toUpperCase();
    return random;
  }

  throwError(error:string){
        document.getElementById('error').style.display = 'block';
    document.getElementById('error').innerHTML = error;
    setTimeout(() => {
      document.getElementById('error').innerHTML = '<br>';
    }, 3000);
  }

  ngOnInit() {
    this.randomFlight = this.generateRandomFlightCode();
    this.locations = JSON.parse(sessionStorage.getItem('destinations'));
  }
  
  redirect(destination) {
    this.router.navigate([destination], {
      skipLocationChange: true
    });
  }
}
