import { Component, OnInit } from '@angular/core';
import { FirebaseDBMService } from 'src/app/firebase-dbm.service';
import {
  Router
} from '@angular/router';
import Joi from '@hapi/joi';

@Component({
  selector: 'app-add-destination',
  templateUrl: './add-destination.component.html',
  styleUrls: ['./add-destination.component.css']
})
export class AddDestinationComponent implements OnInit {

  private submitValidation = Joi.object({
    Destination: Joi.string().required(),
    Code: Joi.string().required()
      .min(3)
      .required()
  });

  constructor(
    private db: FirebaseDBMService,
    private router: Router
    ) { }

  ngOnInit() {
  }

  addDestination(name, ID) {
    const data = {
      Destination: name,
      Code: ID
    };
    const { error } = this.submitValidation.validate(data);
    if (error !== undefined) {
      document.getElementById('error').style.display = 'block';
      document.getElementById('error').innerHTML = error.details[0].message;
      setTimeout(() => {
        document.getElementById('error').innerHTML = '<br>';
      }, 3000);
      return;
    }
    this.db.sendData(data, 'destinations', data.Code);
    document.getElementById('error').style.display = 'block';
    document.getElementById('error').innerHTML = "Destination saved:" + data.Destination + " : " + data.Code;
    setTimeout(() => {
      document.getElementById('error').innerHTML = '<br>';
    }, 3000);
    const form = document.getElementById("loginForm") as HTMLFormElement;
    this.db.getDestinations();
    form.reset();
  }
  
  redirect(destination) {
    this.router.navigate([destination], {
      skipLocationChange: true
    });
  }
}
