import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  adminStatus: string;
  user;
  constructor(
    private router: Router
  ) {}

  ngOnInit() {
    if (JSON.parse(sessionStorage.getItem('userData')) === null) {
      sessionStorage.setItem(
        'userData',
        JSON.stringify({
          username: 'none',
          isAdmin: 'notLogged'
        })
      );

    }
    const loginStatus = this.loginStatus();
    this.adminStatus = this.isAdmin();
    this.user = JSON.parse(sessionStorage.getItem('userData'));
    console.log(this.user);
  }

  logout() {
    sessionStorage.setItem('loggedIn', 'false');
    sessionStorage.setItem('userData', JSON.stringify({
      username: 'none',
      identity:'',
      isAdmin: 'notLogged'
    }));
    this.redirect('/login-register');
  }

  loginStatus() {
    this.user = JSON.parse(sessionStorage.getItem('userData'));
    return sessionStorage.getItem('loggedIn');
  }

  isAdmin() {
    const sessionUserInfo = JSON.parse(sessionStorage.getItem('userData'));
    return sessionUserInfo.isAdmin;
  }

  redirect(destination) {
    this.router.navigate([destination], {
      skipLocationChange: true
    });
  }

}
