import { TestBed } from '@angular/core/testing';

import { FirebaseDBMService } from './firebase-dbm.service';

describe('FirebaseDBMService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FirebaseDBMService = TestBed.get(FirebaseDBMService);
    expect(service).toBeTruthy();
  });
});
