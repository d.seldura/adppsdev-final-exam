import {
  Injectable
} from '@angular/core';
import {  AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FirebaseDBMService {
  returnData;

  constructor(private firestore: AngularFirestore) {}

  sendData(data: any, collection: string, id: string) {
    return new Promise < any > ((resolve, reject) => {
      this.firestore
        .collection(collection)
        .doc(id)
        .set(data)
        .then((res) => {}, (err) => reject(err));
    });
  }

  retrieveLiveUpdate(collection: string) {
    return this.firestore.collection(collection).snapshotChanges();
  }

  getDestinations() {
    let destinations: Array < any > = [];
    this.firestore
      .collection('destinations')
      .ref.get()
      .then((snapshot) => {
        if (snapshot.empty) {
          return console.log('Destinations empty');
        } else {
          snapshot.forEach((doc) => {
            destinations.push(doc.data());
          });
          console.log("Destinations: ", );
          console.log(destinations);
          sessionStorage.setItem('destinations',JSON.stringify(destinations));
        }
      })
      .catch((error) => {
        alert(
          error
        );
      });
  }
}
