import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocateFlightRoundComponent } from './locate-flight-round.component';

describe('LocateFlightRoundComponent', () => {
  let component: LocateFlightRoundComponent;
  let fixture: ComponentFixture<LocateFlightRoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocateFlightRoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocateFlightRoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
