import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { FirebaseDBMService } from 'src/app/firebase-dbm.service';
import { Router } from '@angular/router';

import Joi from '@hapi/joi';
@Component({
  selector: 'app-locate-flight-round',
  templateUrl: './locate-flight-round.component.html',
  styleUrls: ['./locate-flight-round.component.css']
})
export class LocateFlightRoundComponent implements OnInit {
  private createFlightSchema = Joi.object({
    Origin: Joi.string()
      .min(3)
      .required(),
    Destination: Joi.string()
      .min(3)
      .required(),
    ETD: Joi.date().required()
  });

  private findFlightForm;
  private selectedFlight;
  private locations;
  private departureSearchResult;
  private returnSearchResult;
  private outFlight = {origin: {code: '', display:''}, destination: {code: '', display:''} };

  constructor(
    private formBuilder: FormBuilder, 
    private db: AngularFirestore,
    private dbServ: FirebaseDBMService,
    private router: Router
    ) {
    this.findFlightForm = this.formBuilder.group({
      origin: '',
      destination: '',
      etd: ''
    });
    this.selectedFlight = this.formBuilder.group({
      toFlight: '',
      fromFlight: ''
    });
  }

  onSubmit(flightData, data) {
    const flight = {
      Origin: flightData.origin,
      Destination: flightData.destination,
      ETD: flightData.etd
    };
    const { error } = this.createFlightSchema.validate(flight);
    if (error !== undefined) {
      return alert(error.details[0].message);
    }
    if (flight.Destination==flight.Origin)
      return alert("Origin and Destination cannot be the same!");
    let retData = [];
    this.db
      .collection('flights')
      .ref
      .where('Origin', '==', flight.Origin)
      .where('Destination', '==', flight.Destination)
      .where('ETD_date','==',flight.ETD)
      .get()
      .then((snapshot) => {
        if (snapshot.empty) {
          this.departureSearchResult = null;
        } else {
          snapshot.forEach((doc) => {
            retData.push(doc.data());
          });
          for (let x of retData){
            x.ETD_date = (new Date(x.ETD_date+':'+x.ETD_time)).toLocaleDateString() +' '+ (new Date(x.ETD_date+':'+x.ETD_time)).toLocaleTimeString();
            x.ETA_date = (new Date(x.ETA_date+':'+x.ETA_time)).toLocaleDateString() +' '+ (new Date(x.ETA_date+':'+x.ETA_time)).toLocaleTimeString();
          }
          this.departureSearchResult = retData;
          console.log(this.departureSearchResult);
        }
      });
      this.updateReturn(data);
  }

  
  updateReturn(data){
    const flight = {
      Origin: this.outFlight.origin.code,
      Destination: this.outFlight.destination.code,
      ETD: data
    };
    console.log(flight);
    let retData = [];
    this.db
      .collection('flights')
      .ref
      .where('Origin', '==', flight.Origin)
      .where('Destination', '==', flight.Destination)
      .where('ETD_date','==',flight.ETD)
      .get()
      .then((snapshot) => {
        if (snapshot.empty) {
          this.returnSearchResult = null;
        } else {
          snapshot.forEach((doc) => {
            retData.push(doc.data());
          });
          for (let x of retData){
            x.ETD_date = (new Date(x.ETD_date+':'+x.ETD_time)).toLocaleDateString() +' '+ (new Date(x.ETD_date+':'+x.ETD_time)).toLocaleTimeString();
            x.ETA_date = (new Date(x.ETA_date+':'+x.ETA_time)).toLocaleDateString() +' '+ (new Date(x.ETA_date+':'+x.ETA_time)).toLocaleTimeString();
          }
          this.returnSearchResult = retData;
          console.log(this.returnSearchResult);
        }
      });
  }

  display(data1,data2){
    console.log(data1);
    console.log(data2);
  }
  roundSearch(data1,data2){
    return data1 && data2;
  }

  cart(data){
    if (!data.toFlight&&!data.fromFlight)
    return alert("You must select flights for both outbound and return");
    const outFlight = this.searchFlightDataFromResults(data.toFlight, this.departureSearchResult);
    const retFlight = this.searchFlightDataFromResults(data.fromFlight, this.returnSearchResult);
    const balance = outFlight.Price + retFlight.Price;
    if(confirm("Book flights "+outFlight.FlightCode+" and "+retFlight.FlightCode + "? Total balance: " + balance))
    {
      this.setBooking(outFlight);
      this.setBooking(retFlight);
      alert("Booking successful");//add redirect
    }
  }

  searchFlightDataFromResults(searchParam, collection){
    for (let x of collection){
      if (x.FlightCode==searchParam)
        return x;
    }
  }

  originChange(data){
    this.outFlight.destination.display = document.getElementById(data).innerHTML;
    this.outFlight.destination.code = data;
  }
  destinationChange(data){
    this.outFlight.origin.display = document.getElementById(data).innerHTML;
    this.outFlight.origin.code = data;
  }

  ngOnInit() {
    this.locations = JSON.parse(sessionStorage.getItem('destinations'));
  }

  setBooking(flight){
    const random = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 5).toUpperCase();
    const user = JSON.parse(sessionStorage.getItem('userData'));
    let data = {  
      BookingID : random,
      BookDate: new Date(),
      Airline: flight.Airline,
      Destination: flight.Destination,
      ETA_date: flight.ETA_date,
      ETA_time: flight.ETA_time,
      ETD_date: flight.ETD_date,
      ETD_time: flight.ETD_time,
      FlightCode: flight.FlightCode,
      Origin: flight.Origin,
      Price: flight.Price,
      Owner: user.username,
      Name: user.identity
    }
    this.dbServ.sendData(data,"booked-flights",data.BookingID);
  }
}


