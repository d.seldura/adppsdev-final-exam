import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { environment } from 'src/environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FlightPanelComponent } from './adminPanel/flight-panel/flight-panel.component';
import { AddFlightComponent } from './adminPanel/flightCRUD/add-flight/add-flight.component';
import { LoginRegisterComponent } from './frontPanels/login-register/login-register.component';
import { LoginComponent } from './frontPanels/login/login.component';
import { RegisterComponent } from './frontPanels/register/register.component';
import { HomeComponent } from './frontPanels/home/home.component';
import { UserProfileComponent } from './userPanel/user-profile/user-profile.component';
import { BookFlightComponent } from './userPanel/book-flight/book-flight.component';
import { LocateFlightComponent } from './locate-flight/locate-flight.component';
import { AddDestinationComponent } from './adminPanel/add-destination/add-destination.component';
import { LocateFlightRoundComponent } from './locate-flight-round/locate-flight-round.component';
import { ViewFlightUserComponent } from './view-flight-user/view-flight-user.component';
import { UserdataComponent } from './adminPanel/userdata/userdata.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    FlightPanelComponent,
    AddFlightComponent,
    LoginRegisterComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    UserProfileComponent,
    BookFlightComponent,
    LocateFlightComponent,
    AddDestinationComponent,
    LocateFlightRoundComponent,
    ViewFlightUserComponent,
    UserdataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
