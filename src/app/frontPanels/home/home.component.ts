import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  adminStatus: string;

  constructor(private router: Router) {}

  ngOnInit() {
    if (JSON.parse(sessionStorage.getItem('userData')) === null) {
      sessionStorage.setItem(
        'userData',
        JSON.stringify({ username: 'none', isAdmin: 'notLogged' })
      );
    }
    if (this.isAdmin() === 'true') {
      this.router.navigate(['/adminPanel'], { skipLocationChange: true });
    } else if (this.isAdmin() === 'false') {
      this.router.navigate(['/userPanel'], { skipLocationChange: true });
    }
  }

  isAdmin() {
    const sessionUserInfo = JSON.parse(sessionStorage.getItem('userData'));
    return sessionUserInfo.isAdmin;
  }
}
