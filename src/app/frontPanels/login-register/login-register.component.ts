import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login-register',
  templateUrl: './login-register.component.html',
  styleUrls: ['./login-register.component.css']
})
export class LoginRegisterComponent implements OnInit {
  page: string;

  constructor() {}

  ngOnInit() {
    this.page = 'login';
  }

  setPage(data: string) {
    this.page = data;
  }

  getPage() {
    return this.page;
  }
}
