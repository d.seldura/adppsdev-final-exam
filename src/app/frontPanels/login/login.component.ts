import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { FirebaseDBMService } from 'src/app/firebase-dbm.service';
import Joi from '@hapi/joi';
import bcryptjs from 'bcryptjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private loginSchema = Joi.object(
    {
    Email: Joi.string().required(),
    Password: Joi.string()
      .min(3)
      .required()
    }
  );

  private loginForm;

  constructor(
    private formBuilder: FormBuilder,
    private firestore: AngularFirestore,
    private db: FirebaseDBMService,
    private router: Router
  ) {
    this.loginForm = this.formBuilder.group({
      email: '',
      password: ''
    });
  }

  ngOnInit(): void {}

  onSubmit(userData) {
    const user = {
      Email: userData.email,
      Password: userData.password
    };
    const { error } = this.loginSchema.validate(user);
    if (error !== undefined) {
      return alert(error.details[0].message);
    }
    let retData;
    this.firestore
      .collection('users')
      .doc(userData.email)
      .ref.get()
      .then((doc) => {
        if (doc.exists) {
          return (retData = doc.data());
        } else {
          throw new Error('User not found');
        }
      })
      .then(async () => {
        const validPass = await bcryptjs.compare(user.Password, retData.Password);
        if (!validPass) {
          throw new Error('Invalid Password');
        } else {
          // successful login
          sessionStorage.setItem('loggedIn', 'true');
          sessionStorage.setItem('userData',JSON.stringify({ username: user.Email, identity: retData.LastName+','+retData.FirstName, isAdmin: retData.isAdmin })
          );
          this.db.getDestinations();
          this.router.navigate(['/'], { skipLocationChange: true });
        }
      })
      .catch((error) => {
        alert(
          error
        );
      });
  }

  onReset() {
    const resetForm = document.getElementById('loginForm') as HTMLFormElement;
    resetForm.reset();
  }
}
