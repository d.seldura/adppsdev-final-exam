import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { FirebaseDBMService } from 'src/app/firebase-dbm.service';
import Joi from '@hapi/joi';
import bcryptjs from 'bcryptjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  ngOnInit(): void {
  }

  private createUserSchema = Joi.object({
    Email: Joi.string()
      .required(),
    Password: Joi.string()
      .min(3)
      .required(),
    FirstName: Joi.string()
    .min(3)
    .required(),
    LastName: Joi.string()
    .min(3)
    .required(),
    dateOfBirth: Joi.date().required(),
    gender: Joi.string().required(),
    isAdmin: Joi.string()
  });

  private userForm;

  constructor(private formBuilder: FormBuilder, private db: FirebaseDBMService, private router: Router) { 
    this.userForm = this.formBuilder.group({
      email: '',
      password: '',
      firstName: '',
      lastName: '',
      dateOfBirth: '',
      gender: '' 
    });
  };  

  onSubmit(userData) {
    const salt = bcryptjs.genSaltSync(10);
    const hashedPassword = bcryptjs.hashSync(userData.password, salt);
    let user = {
      Email: userData.email,
      Password: hashedPassword,
      FirstName: userData.firstName,
      LastName: userData.lastName,
      dateOfBirth: userData.dateOfBirth,
      gender: userData.gender,
      isAdmin: 'false'
    };
    const { error } = this.createUserSchema.validate(user);
    if (error!=undefined) return alert(error.details[0].message);
    this.db.sendData(user, 'users', user.Email);
    alert("User Added");
    this.onReset();
    this.router.navigate(['/login'], { skipLocationChange: true });
    }

  onReset(){
    const resetForm = document.getElementById('userForm') as HTMLFormElement;
    resetForm.reset();
  }

}
